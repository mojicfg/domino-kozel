#pragma once

#include "engine.hpp"

namespace ui {
	enum struct which_side_t {
		left,
		right,
	};
	bool ask_next_game();
	void prompt_first_turn_double(int);
	which_side_t prompt_which_side();
	void display_game_state();
	engine::stone_t prompt_which_stone();
	void notify_empty_stock();

	int prompt_player_amount();
	void notify_first_player(int);
};

