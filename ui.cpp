#include <cassert>
#include <iostream>
#include <sstream>
#include <string>

#include "ui.hpp"

namespace ui {
	std::string stone_to_string(int a, int b) {
		std::stringstream ss;
		ss << "[" << a << ":" << b << "]";
		return ss.str();
	}
	std::string stone_to_string(engine::stone_t stone) {
		return stone_to_string(stone.first, stone.second);
	}
	std::string double_stone_to_string(int a) {
		return stone_to_string(a, a);
	}

	bool ask_next_game() {
		std::string response;
		while (response.length() == 0 || (response[0] != 'y' && response[0] != 'Y' &&
					response[0] != 'n' && response[0] != 'N')) {
			fputs("Play next game? (y/n) ", stdout);
			std::getline(std::cin, response);
		}
		return response[0] == 'y' || response[0] == 'Y';
	}

	void prompt_first_turn_double(int double_value) {
		std::cout << "You have a double " << double_stone_to_string(double_value);
		std::cout << "." << std::endl;
		std::string response;
		int amt = 0;
#define MAX_EXCLAMATIONS 3
		while (response != "y" && response != "Y") {
			std::cout << "Do you want to play it?";
			for (int i = 0; i < amt; ++i) {
				std::cout << "!";
			}
			std::cout << " (y/n) ";
			std::getline(std::cin, response);
			++amt;
			amt = std::min(amt, MAX_EXCLAMATIONS);
		}
	}

	which_side_t prompt_which_side() {
		std::cout << "Which side to play? (L/R) ";
		std::string response;
		while (response.empty() || (response[0] != 'l' && response[0] != 'L' &&
					response[0] != 'r' && response[0] != 'R')) {
			std::getline(std::cin, response);
		}
		if (response[0] == 'l' || response[0] == 'L')
			return which_side_t::left;
		return which_side_t::right;
	}

	void display_board() {
		const engine::board_state_t board_state = engine::get_board_state();
		std::cout << "L:" << board_state.chain_left << " ";
		std::cout << "R:" << board_state.chain_right << std::endl;
	}
	void display_hand() {
		const engine::hand_t hand = engine::get_current_player_hand();
		for (const engine::stone_t &stone: hand) {
			std::cout << stone_to_string(stone) << " ";
		}
		std::cout << std::endl;
	}
	void display_game_state() {
		display_board();
		display_hand();
	}

	engine::stone_t prompt_which_stone() {
		engine::stone_t stone;
		while (true) {
			std::cout << "Which stone would you like to play? ";
			// XXX the input method below is stupid
			std::cin >> stone.first >> stone.second;
			std::string tmp;
			std::getline(std::cin, tmp);
			if (stone == engine::pass_stone || stone == engine::pull_stock_stone) {
				return stone;
			}
			if (engine::is_stone_in_current_player_hand(stone)) {
				if (engine::is_stone_valid_to_play(stone)) {
					return stone;
				} else {
					std::cout << "You cannot play this stone!" << std::endl;
				}
			} else {
				std::cout << "You don't have this stone!" << std::endl;
			}
		}
		assert(false);
	}

	void notify_empty_stock() {
		std::cout << "The stock is empty!" << std::endl;
	}

	int prompt_player_amount() {
		int amt = 0;
		do {
			std::cout << "Number of players: ";
			// XXX the input method below is stupid
			std::cin >> amt;
			std::string tmp;
			std::getline(std::cin, tmp);
		} while(!engine::is_valid_number_of_players(amt));
		return amt;
	}

	void notify_first_player(int current_player) {
		std::cout << "Player " << current_player << " to begin." << std::endl;
	}
};

