#pragma once
#include <set>
#include <vector>
#include <utility> // std::pair

#define MIN_PLAYERS 2
#define MAX_PLAYERS 4
#define MAX_PIPS 7 // [0; MAX_PIPS) <- non-inclusive
#define INITIAL_HAND_SIZE 7

namespace engine {
	typedef std::pair<int,int> stone_t;
	typedef std::vector<stone_t> stock_t;
	typedef std::set<stone_t> hand_t;
	const stone_t pass_stone = {-1, -1};
	const stone_t pull_stock_stone = {-2, -2};

	void init(int = MIN_PLAYERS);
	void make_first_turn();
	void make_turn();

	bool is_game_going();

	struct board_state_t {
		int chain_left, chain_right;
	};
	board_state_t get_board_state();
	hand_t get_current_player_hand();

	bool is_stone_in_current_player_hand(stone_t&);
	bool is_stone_valid_to_play(const stone_t);

	bool is_valid_number_of_players(const int);
};

