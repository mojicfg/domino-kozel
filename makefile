CXX=g++
CXXFLAGS=-std=c++17

app: engine.o main.o ui.o
	$(CXX) $^ -o$@

main: main.cpp engine.hpp ui.hpp
	$(CXX) $(CXXFLAGS) -c $<

engine: engine.cpp engine.hpp ui.hpp
	$(CXX) $(CXXFLAGS) -c $<

ui: ui.cpp engine.hpp ui.hpp
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -f *.o

