#include "engine.hpp"
#include "ui.hpp"

int main() {
	const int player_amt = ui::prompt_player_amount();
	do {
		engine::init(player_amt);
		engine::make_first_turn();
		while (engine::is_game_going()) {
			engine::make_turn();
		}
	} while (ui::ask_next_game());
}

