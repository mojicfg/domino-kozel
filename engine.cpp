#include <algorithm> // std::shuffle, std::swap
#include <chrono>
#include <random> // std::mt19937

#include "engine.hpp"
#include "ui.hpp"

namespace engine {

	std::mt19937 rng(std::chrono::steady_clock::now().time_since_epoch().count());

	stock_t stock;
	hand_t hand[MAX_PLAYERS];

	int player_amt;
	int current_player; // to turn
	int consecutive_passes;

	int chain_left, chain_right;
#define EMPTY_CHAIN_ENDS -1

	void init_stock() {
		stock.clear();
		for (int i = 0; i < MAX_PIPS; ++i) {
			for (int j = 0; j <= i; ++j) {
				stock.push_back({j, i});
			}
		}
		std::shuffle(stock.begin(), stock.end(), rng);
	}
	void init_hands() {
		for (int i = 0; i < player_amt; ++i) {
			hand[i].clear();
			for (int j = 0; j < INITIAL_HAND_SIZE; ++j) {
				hand[i].insert(stock.back());
				stock.pop_back();
			}
		}
	}

	int pick_first_player() {
		// find smallest double
		// (0:0 treated as highest and represented as `MAX_PIPS`:`MAX_PIPS`)
		// double-not-found is also represented with `best_found_double` = `MAX_PIPS`
		int first_player = 0; // TODO pick random index here
		int best_found_double = MAX_PIPS;
		for (int i = 0; i < player_amt; ++i) {
			for (const stone_t &stone: hand[i]) {
				if (stone.first == stone.second) {
					const int found_double = stone.first == 0? MAX_PIPS: stone.first;
					if (found_double <= best_found_double) {
						first_player = i;
						best_found_double = found_double;
					}
				}
			}
		}
		return first_player;
	}

	void pick_next_player() {
		++current_player;
		current_player %= player_amt;
	}

	void init(int player_amt) {
		player_amt = std::min(player_amt, MAX_PLAYERS);
		player_amt = std::max(player_amt, MIN_PLAYERS);
		engine::player_amt = player_amt;
		init_stock();
		init_hands();
		engine::current_player = pick_first_player();
		chain_left = chain_right = EMPTY_CHAIN_ENDS;
		consecutive_passes = 0;
	}

	void play_first_stone(const stone_t stone) {
		hand[current_player].erase(stone);
		chain_left = stone.first;
		chain_right = stone.second;
	}
	void play_stone(const stone_t stone) {
		hand[current_player].erase(stone);
		ui::which_side_t side;
		if (std::min(chain_left, chain_right) == std::min(stone.first, stone.second) &&
				std::max(chain_left, chain_right) == std::max(stone.first, stone.second)) {
			side = ui::prompt_which_side();
		} else {
			if (chain_left == stone.first || chain_left == stone.second) {
				side = ui::which_side_t::left;
			} else {
				side = ui::which_side_t::right;
			}
		}
		if (side == ui::which_side_t::left) {
			// change `chain_left` to the second value
			chain_left = stone.first + stone.second - chain_left;
		} else {
			// change `chain_right` to the second value
			chain_right = stone.first + stone.second - chain_right;
		}
	}

	void pull_stock() {
		if (stock.empty()) {
			ui::notify_empty_stock();
			return;
		}
		hand[current_player].insert(stock.back());
		stock.pop_back();
		ui::display_game_state();
	}

	void make_turn() {
		ui::display_game_state();
		stone_t stone;
		while ((stone = ui::prompt_which_stone()) == pull_stock_stone) {
			pull_stock();
		}
		if (stone != pass_stone) {
			play_stone(stone);
			consecutive_passes = 0;
		} else {
			++consecutive_passes;
		}
		pick_next_player();
	}

	void make_first_turn() {
		int best_found_double = MAX_PIPS + 1; // `MAX_PIPS` represents 0:0
		for (const stone_t &stone: hand[current_player]) {
			if (stone.first == stone.second) {
				const int found_double = stone.first == 0? MAX_PIPS : stone.first;
				best_found_double = std::min(best_found_double, found_double);
			}
		}
		ui::notify_first_player(current_player);
		if (best_found_double == MAX_PIPS + 1) { // aka no double found
			make_turn();
		} else {
			if (best_found_double == MAX_PIPS) {
				best_found_double = 0;
			}
			ui::prompt_first_turn_double(best_found_double);
			play_first_stone({best_found_double, best_found_double});
			pick_next_player();
		}
	}

	bool is_game_going() {
		if (consecutive_passes == player_amt) {
			// the fish wins
			return false;
		}
		const int prev_player = ((current_player - 1) % player_amt + player_amt) % player_amt;
		if (hand[prev_player].empty()) {
			// empty hand wins
			return false;
		}
		return true;
	}

	board_state_t get_board_state() {
		return {chain_left, chain_right};
	}

	hand_t get_current_player_hand() {
		// XXX check for return-by-copy vs return-by-reference
		return hand[current_player];
	}

	bool is_stone_in_current_player_hand(stone_t &stone) {
		if (stone.first > stone.second) {
			std::swap(stone.first, stone.second);
		}
		return hand[current_player].find(stone) != hand[current_player].end();
	}

	bool is_stone_valid_to_play(const stone_t stone) {
		return (chain_left == stone.first || chain_left == stone.second ||
				chain_right == stone.first || chain_right == stone.second);
	}

	bool is_valid_number_of_players(const int player_amt) {
		return MIN_PLAYERS <= player_amt && player_amt <= MAX_PLAYERS;
	}
};

